import com.jme3.math.Vector3f;
import com.jme3.texture.Image;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Point3D;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.videoio.VideoCapture;

import java.util.Optional;

public class CameraBackgroundService implements Runnable {

    private FaceDetectionService faceDetectionService = new FaceDetectionService();
    private ObjectProperty<Image> imageObjectProperty;
    private ObjectProperty<Vector3f> headPosition;
    private VideoCapture capture;

    CameraBackgroundService(ObjectProperty<Image> imageObjectProperty, ObjectProperty<Vector3f> headPosition, VideoCapture capture) {
        this.imageObjectProperty = imageObjectProperty;
        this.headPosition = headPosition;
        this.capture = capture;
    }

    @Override
    public void run() {
        Mat frame = grabFrame(capture);
        Image imageToShow = Utils.mat2JMEImage(frame);
        imageObjectProperty.setValue(imageToShow);
    }

    public Mat grabFrame(VideoCapture capture) {
        Mat frame = new Mat();
        if (capture.isOpened()) {
            try {
                capture.read(frame);
                if (!frame.empty()) {
                    Optional<Rect> face = faceDetectionService.detectFaceAndDrawRectAround(frame);
                    if(face.isPresent()) {
                        Point3D headShift = HeadShiftService.calculateHeadPosition(face.get());
                        headPosition.set(Utils.point3dToVector3f(headShift));
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return frame;
    }
}
