import javafx.geometry.Point3D;

public class ConfigurationService {
    public static double cameraHorizontalViewAngle = Math.PI / 3;
    public static int cameraHorizontalResolution = 640;
    public static int cameraVerticalResolution = 320;
    public static double realHeadWidth = 150;
    public static int screenWidth = 310;
    public static int screenHeight = 175;
    public static int screenVerticalResolution = 768;
    public static int screenHorizontalResolution = 1366;
    public static Point3D screenRelativeCameraShift = new Point3D(0, -screenHeight / 2 - 15, 0);
}
