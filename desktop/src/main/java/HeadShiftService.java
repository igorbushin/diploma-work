import javafx.geometry.Point3D;
import org.opencv.core.Rect;

public class HeadShiftService {
    public static Point3D calculateHeadPosition(Rect headRect) {
        double tanHalfViewAngle = Math.tan(ConfigurationService.cameraHorizontalViewAngle / 2);
        double headZinCameraWorld = ConfigurationService.cameraHorizontalResolution / 2 / tanHalfViewAngle;
        double headXinCameraWorld = headRect.x + headRect.width / 2 - ConfigurationService.cameraHorizontalResolution / 2;
        double headYinCameraWorld = -(headRect.y + headRect.height / 2 - ConfigurationService.cameraVerticalResolution / 2);
        double cameraToRealityScale = ConfigurationService.realHeadWidth / headRect.width;
        Point3D headPositionInCameraWorld = new Point3D(headXinCameraWorld, headYinCameraWorld, headZinCameraWorld);
        Point3D headPositionInRealWorld = headPositionInCameraWorld.multiply(cameraToRealityScale);
        return headPositionInRealWorld.subtract(ConfigurationService.screenRelativeCameraShift);
    }
}
