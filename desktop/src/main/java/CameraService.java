import com.jme3.math.Vector3f;
import com.jme3.texture.Image;
import javafx.beans.property.ObjectProperty;
import org.opencv.videoio.VideoCapture;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CameraService {

    private ObjectProperty<Image> imageObjectProperty;
    private ObjectProperty<Vector3f> headPosition;
    private VideoCapture capture = new VideoCapture();
    private Optional<ScheduledExecutorService> timer = Optional.empty();

    CameraService(ObjectProperty<Image> imageObjectProperty, ObjectProperty<Vector3f> headPosition){
        this.imageObjectProperty = imageObjectProperty;
        this.headPosition = headPosition;
    }

    public void stopCamera() {
        if (timer.isPresent() && !timer.get().isShutdown()) {
            timer.get().shutdown();
            try {
                timer.get().awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (capture.isOpened()) {
            capture.release();
        }
    }

    public void startCamera(){
        capture.open(0);
        if(capture.isOpened()) {
            timer = Optional.of(Executors.newSingleThreadScheduledExecutor());
            CameraBackgroundService cameraBackgroundService = new CameraBackgroundService(imageObjectProperty, headPosition, capture);
            timer.get().scheduleAtFixedRate(cameraBackgroundService, 0, 33, TimeUnit.MILLISECONDS);
        }
    }
}
