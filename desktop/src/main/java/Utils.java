import com.jme3.math.Vector3f;
import com.jme3.texture.Image;
import com.jme3.util.BufferUtils;
import javafx.geometry.Point3D;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.nio.ByteBuffer;

public final class Utils
{
    public static Vector3f point3dToVector3f(Point3D point3D) {
        float x = (float)point3D.getX();
        float y = (float)point3D.getY();
        float z = (float)point3D.getZ();
        return new Vector3f(x, y, z);
    }

    public static Image mat2JMEImage(Mat mat) {

        BufferedImage bufferedImage = Utils.matToBufferedImage(mat);
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        ByteBuffer byteBuffer = Utils.bufferedImage2byteBuffer(bufferedImage);
        return new Image(com.jme3.texture.Image.Format.RGB8, w, h, byteBuffer);
    }

    public static ByteBuffer bufferedImage2byteBuffer(BufferedImage image) {

        int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 3);

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[(image.getHeight() - 1 - y) * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
                buffer.put((byte) (pixel & 0xFF)); // Blue component
            }
        }

        buffer.flip();
        return buffer;

    }

    public static BufferedImage matToBufferedImage(Mat original)
    {
        // init
        BufferedImage image = null;
        int width = original.width(), height = original.height(), channels = original.channels();
        byte[] sourcePixels = new byte[width * height * channels];
        original.get(0, 0, sourcePixels);

        if (original.channels() > 1)
        {
            image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        }
        else
        {
            image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        }
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);

        return image;
    }
}