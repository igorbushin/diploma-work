import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.texture.Image;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import javafx.beans.property.SimpleObjectProperty;
import nu.pattern.OpenCV;

public class Main extends SimpleApplication {

    public static void main(String[] args){
        OpenCV.loadLocally();
        Main app = new Main();
        app.start();
    }

    private SimpleObjectProperty<Image> imageProperty;
    private SimpleObjectProperty<Vector3f> headPosition;
    private CameraService cameraService;
    private Spatial teapot;
    private Picture hudPicture;
    private BitmapText hudText;

    @Override
    public void simpleInitApp() {

        headPosition = new SimpleObjectProperty<>();
        imageProperty = new SimpleObjectProperty<>();

        cameraService = new CameraService(imageProperty, headPosition);
        cameraService.startCamera();

        teapot = assetManager.loadModel("Models/Teapot.j3o");
        Material mat_default = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");
        teapot.setMaterial(mat_default);
        teapot.setLocalTranslation(0,-50,0);
        teapot.scale(150);
        rootNode.attachChild(teapot);

        hudPicture = new Picture("HUD Picture");
        hudPicture.setWidth(settings.getWidth()/4);
        hudPicture.setHeight(settings.getHeight()/4);
        hudPicture.setPosition(settings.getWidth() * 3 / 4, 0);
        hudPicture.setImage(assetManager, "Textures/jme-logo.png", true);
        guiNode.attachChild(hudPicture);

        hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize());
        hudText.setColor(ColorRGBA.Red);
        hudText.setText("text");
        hudText.setLocalTranslation(settings.getWidth() * 3 / 4, hudText.getLineHeight()*3, 0);
        guiNode.attachChild(hudText);
    }

    @Override
    public void simpleUpdate(float tpf) {

        if(imageProperty.get() != null) {
            Texture2D texture = new Texture2D(imageProperty.get());
            hudPicture.setTexture(assetManager, texture, true);
        }
        if(headPosition.get() != null) {
            Vector3f head = new Vector3f(-headPosition.get().getX(), headPosition.get().getY(), headPosition.get().getZ());

            float left = (-ConfigurationService.screenWidth / 2 - head.x) / head.z;
            float right = (ConfigurationService.screenWidth / 2 - head.x) / head.z;
            float top = (ConfigurationService.screenHeight / 2 - head.y) / head.z;
            float bottom = (-ConfigurationService.screenHeight / 2 - head.y) / head.z;
            cam.setFrustum(1, 100000, left, right, top, bottom);

            Vector3f lookPoint = new Vector3f(head.x, head.y, 0);
            cam.lookAt(lookPoint, Vector3f.UNIT_Y);
            cam.setLocation(head);

            String text = "X:" + (int)headPosition.get().getX() + "\n" +
                    "Y:" + (int)headPosition.get().getY() + "\n" +
                    "Z:" + (int)headPosition.get().getZ() + "\n" +
                    "L:" + left + "\n" +
                    "R:" + right + "\n" +
                    "T:" + top + "\n" +
                    "B:" + bottom + "\n";
            hudText.setText(text);
            hudText.setLocalTranslation(settings.getWidth() * 3 / 4, hudText.getLineHeight()*7, 0);
        }
    }

    @Override
    public void destroy() {
        cameraService.stopCamera();
        super.destroy();
    }
}