import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import java.util.Optional;

public class FaceDetectionService {

    private String lbp = "lbpcascade_frontalface.xml";
    private String haarAlt = "haarcascade_frontalface_alt.xml";
    private String haarAlt2 = "haarcascade_frontalface_alt2.xml";
    private String haarAltTree = "haarcascade_frontalface_alt_tree.xml";
    private String haarDef = "haarcascade_frontalface_default.xml";
    private CascadeClassifier cascadeClassifier = new CascadeClassifier();

    FaceDetectionService() {
        cascadeClassifier.load("desktop\\src\\main\\resources\\cascades\\" + haarAlt2);
    }

    public Optional<Rect> detectFaceAndDrawRectAround(Mat frame) {
        MatOfRect faces = new MatOfRect();
        Mat grayFrame = new Mat();
        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(grayFrame, grayFrame);
        int frameHeight = grayFrame.rows();
        int minimalFaceSize = Math.round(frameHeight * 0.2f);
        Size faceSize = new Size(minimalFaceSize, minimalFaceSize);
        int flags = Objdetect.CASCADE_FIND_BIGGEST_OBJECT | Objdetect.CASCADE_SCALE_IMAGE;
        cascadeClassifier.detectMultiScale(grayFrame, faces, 1.1, 2, flags, faceSize , new Size());
        Rect [] facesArray = faces.toArray();
        if(facesArray.length > 0) {
            Rect perfectFace = facesArray[0];
            for (Rect face : facesArray) {
                if(face.width * face.height > perfectFace.width * perfectFace.height) {
                    perfectFace = face;
                }
            }
            Imgproc.rectangle(frame, perfectFace.tl(), perfectFace.br(), new Scalar(0, 255, 0), 3);
            return Optional.of(perfectFace);
        }
        return Optional.empty();
    }
}
